# Tailwind Toolkit

High-level CSS components for Tailwind

## Goals

- be a toolkit for writing applications with building blocks, keeping the possibility to add more fine-grained design
- give access to CSS components and JS components
- respect Tailwind principles
- have themes (colors, variants), all components being consistent across them
- be generic enough (contrary to most design systems dedicated to one company)
- have common components found in most UI toolkits like Bootstrap
- rely on plugins instead of having one monolithic project
- use Svelte JS library because it compiles to vanilla JS
- have excellent accessibility (WCAG contrasts)

Each plugin should have:

- a good README (minimal Tailwind version, install, usage)
- an image preview
- a [Storybook](https://storybook.js.org/) demo
- tests (e.g. https://github.com/webdna/tailwindcss-aspect-ratio/blob/master/test.js)

Variants:

- loading state
