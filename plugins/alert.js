const plugin = require("tailwindcss/plugin")


const PLUGIN_NAME = "alert"
const CLASS_PREFIX = PLUGIN_NAME


module.exports = plugin(function ({ addComponents, theme, e }) {
  const alert = {
    [`.${e(CLASS_PREFIX)}`]: {
      borderWidth: theme("borderWidth.default"),
      borderRadius: theme("borderRadius.default"),
      padding: `${theme("spacing.3")} ${theme("spacing.4")}`,
      position: "relative",
    },
    [`.${e(`${CLASS_PREFIX}-dismissible`)}`]: {
      paddingRight: "1em",
      ".close": {
        padding: `${theme("spacing.3")} ${theme("spacing.4")}`,
        position: "absolute",
        bottom: 0,
        right: 0,
        top: 0,
        "svg": {
          height: theme("spacing.6"),
          fill: theme("fill.current"),
          width: theme("spacing.6"),
        }
      }
    }
  }

  addComponents([alert])
}, {
  theme: {
    [PLUGIN_NAME]: {}
  }
})
