const _ = require("lodash")
const Color = require("color")

const defaultTheme = require("tailwindcss/defaultTheme")
const plugin = require("tailwindcss/plugin")


const PLUGIN_NAME = "button"
const CLASS_PREFIX = "btn"


module.exports = plugin(function ({ addComponents, theme, e }) {
  const themeColors = theme(`${PLUGIN_NAME}.colors`, {})
  const themeSizes = theme(`${PLUGIN_NAME}.sizes`, {})

  const baseButton = {
    [`.${e(CLASS_PREFIX)}`]: {
      borderRadius: theme("borderRadius.default"),
      fontWeight: theme("fontWeight.semibold"),
      fontSize: themeSizes.default.fontSize,
      padding: themeSizes.default.padding,
      "&:disabled": {
        cursor: theme("cursor.not-allowed"),
        opacity: theme("opacity.75"),
      },
      "&:focus": {
        boxShadow: theme("boxShadow.outline"),
        outline: 0,
      },
    }
  }

  const defaultButtons = _.map(themeColors, (value, modifier) => ({
    [`.${e(`${CLASS_PREFIX}-${modifier}`)}`]: {
      backgroundColor: value.background,
      border: "1px solid transparent",
      color: value.foreground,
      "&:hover:enabled": {
        backgroundColor: Color(value.background).darken(0.2).hex(),
      },
      "&:active": {
        backgroundColor: Color(value.background).darken(0.4).hex()
      }
    }
  }))

  const outlineButtons = _.map(themeColors, (value, modifier) => ({
    [`.${e(`${CLASS_PREFIX}-outline`)}.${e(`${CLASS_PREFIX}-${modifier}`)}`]: {
      backgroundColor: "transparent",
      border: "1px solid",
      color: value.background,
      "&:hover:enabled": {
        backgroundColor: value.background,
        color: value.foreground,
      },
      "&:active": {
        backgroundColor: Color(value.background).darken(0.4).hex()
      }
    }
  }))

  const pillButton = {
    [`.${e(`${CLASS_PREFIX}-pill`)}`]: {
      borderRadius: theme("borderRadius.full"),
    }
  }

  const buttonSizes = _(themeSizes)
    .omit("default")
    .map((value, modifier) => ({
      [`.${e(`${CLASS_PREFIX}-${modifier}`)}`]: {
        fontSize: value.fontSize,
        padding: value.padding,
      }
    }))
    .value()

  addComponents([baseButton, defaultButtons, outlineButtons, pillButton, buttonSizes])
}, {
  theme: {
    [PLUGIN_NAME]: {
      colors: {
        primary: {
          background: defaultTheme.colors.blue["500"],
          foreground: defaultTheme.colors.white,
        },
        secondary: {
          background: defaultTheme.colors.gray["500"],
          foreground: defaultTheme.colors.black,
        },
      },
      sizes: {
        sm: {
          fontSize: defaultTheme.fontSize.sm,
          padding: `${defaultTheme.spacing["1"]} ${defaultTheme.spacing["2"]}`,
        },
        default: {
          fontSize: defaultTheme.fontSize.base,
          padding: `${defaultTheme.spacing["2"]} ${defaultTheme.spacing["4"]}`,
        },
        lg: {
          fontSize: defaultTheme.fontSize.xl,
          padding: `${defaultTheme.spacing["2"]} ${defaultTheme.spacing["6"]}`,
        }
      },
    }
  }
})
