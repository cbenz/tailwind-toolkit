const { colors } = require("tailwindcss/defaultTheme")

const alert = require("./plugins/alert.js")
const button = require("./plugins/button.js")

const buttonDefaultTheme = button.config.theme.button

module.exports = {
  theme: {
    extend: {
      // colors: { red: colors.blue },
      // alert: {
      //   colors: {
      //     primary: {}
      //   }
      // },
      button: {
        colors: {
          ...buttonDefaultTheme.colors,
          secondary: {
            background: colors.yellow[500],
            foreground: colors.red[500],
          },
          boogie: {
            background: colors.green[400],
            foreground: colors.pink[400],
          },
        }
      }
    },
  },
  variants: {},
  plugins: [alert, button],
}
